<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
use Dompdf\Dompdf;
function pdf_create($html, $filename='', $stream=TRUE, $paper = 'Letter', $orientation = 'portrait') 
{

    require_once("pdf/autoload.php");
    
    $dompdf = new DOMPDF();
    $dompdf->load_html($html);
    $dompdf->set_paper($paper, $orientation);

    $dompdf->render();
    if ($stream) {
        $dompdf->stream($filename.'.pdf', array("Attachment" => 0));
    } else {
        return $dompdf->output(); 
    }
}

/* End of file dompdf_helper.php */
/* Location: ./application/helpers/dompdf_helper.php */
