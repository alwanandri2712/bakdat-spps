<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Media
|--------------------------------------------------------------------------
| Konfigurasi untuk lokasi media
| 
|
*/
$config['media_url'] = '-';

/*
|--------------------------------------------------------------------------
| Setting
|--------------------------------------------------------------------------
|
| 
|
*/
$config['app_name'] = 'SPP-Sekolah ';
$config['version'] = 'V.3.0';
$config['created'] = 'Copyright © 2017-'.date('Y').' <a target="_blank" href="#">SPP-Sekolah™</a>. All rights reserved.';

/*
|--------------------------------------------------------------------------
| Email
|--------------------------------------------------------------------------
|
| Configuration email to send email notification
|
*/
$config['email'] = TRUE;

/*
|--------------------------------------------------------------------------
| Email disclaimer
|--------------------------------------------------------------------------
|
*/
$config['email_disclaimer'] = "\n\n<em>Email ini dikirim secara otomatis, mohon untuk tidak membalas email ini. Jika mengalami kendala teknis maka dapat menghubungi";
