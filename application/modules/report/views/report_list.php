
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo isset($title) ? '' . $title : null; ?>
			<small>List</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('manage') ?>"><i class="fa fa-th"></i> Home</a></li>
			<li class="active"><?php echo isset($title) ? '' . $title : null; ?></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-success">
					<div class="box-header">
						<?php echo form_open(current_url(), array('method' => 'get')) ?> <br>
						<div class="row">
							<div class="col-md-2">  
								<div class="form-group">
									<div class="input-group date " data-date="" data-date-format="yyyy-mm-dd">
										<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
										<input class="form-control" type="text" name="ds" readonly="readonly" <?php echo (isset($q['ds'])) ? 'value="'.$q['ds'].'"' : '' ?> placeholder="Tanggal Awal">
									</div>
								</div>
							</div>
							<div class="col-md-2">  
								<div class="form-group">
									<div class="input-group date " data-date="" data-date-format="yyyy-mm-dd">
										<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
										<input class="form-control" type="text" name="de" readonly="readonly" <?php echo (isset($q['de'])) ? 'value="'.$q['de'].'"' : '' ?> placeholder="Tanggal Akhir">
									</div>
								</div>
							</div>
							<div class="col-md-2">  
								<div class="form-group">
									<!-- <label>Unit</label> -->
									<select class="form-control" name="k">
										<?php foreach ($majors as $row): ?>
											<option <?php echo (isset($q['k']) AND $q['k'] == $row['majors_id']) ? 'selected' : '' ?> value="<?php echo $row['majors_id'] ?>"><?php echo $row['majors_name'] ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="col-md-2">  
								<div class="form-group">
									<!-- <label>Unit</label> -->
									<select class="form-control" name="c">
										<?php foreach ($class as $cls): ?>
											<option <?php echo (isset($q['c']) AND $q['c'] == $cls['class_id']) ? 'selected' : '' ?> value="<?php echo $cls['class_id'] ?>"><?php echo $cls['class_name'] ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="col-md-3">
								<button type="submit" class="btn btn-primary">Filter</button>
								<?php if ($q) { ?>
								<a class="btn btn-success" href="<?php echo site_url('manage/report/report' . '/?' . http_build_query($q)) ?>" target="_blank"><i class="fa fa-file-excel-o" ></i> Export Data</a>
								<!--<a class="btn btn-success" formtarget="_blank" href="<?php echo site_url('manage/report/cetakreport' . '/?' . http_build_query($q)) ?>"><i class="fa fa-file-excel-o" ></i> Export Excel</a>
								-->
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
				<div class="box box-info">
					<div class="box-body table-responsive">
						<table class="table table-responsive table-hover table-bordered" style="white-space: nowrap;">
			<thead>
				<tr>
					<th>NO</th>
					<th>PEMBAYARAN</th>
					<th>NAMA SISWA</th>
					<th>UNIT</th>
					<th>KELAS</th>
					<th>TANGGAL</th>
					<th>PENERIMAAN</th>
					<th>PENGELUARAN</th>
					<th>KETERANGAN</th>
				</tr>		
			</thead>
			<tbody>
				<?php 
				$no=1;
				$masuk = 0;
				$keluar = 0;
				foreach ($bulan as $key) { 
				$masuk = $masuk+$key['bulan_bill'];	
				?>
 				<tr>
 					<td><?=$no;?></td>
 					<td><?=$key['pos_name'].' - T.A '.$key['period_start'].'/'.$key['period_end'].'-'.$key['month_name']?></td>
 					<td><?=$key['student_full_name']?></td>
 					<td><?=$key['majors_short_name']?></td>
 					<td><?=$key['class_name']?></td>
 					<td><?=pretty_date($key['bulan_date_pay'], 'm/d/Y', FALSE)?></td>
 					<td>Rp. <?=number_format($key['bulan_bill'], 0, ',', '.')?></td>
 					<td>-</td>
 					<td>-</td>
 				</tr>		
				<?php	
				$no++;	
				}
				?>
				<?php
				foreach ($free as $row) { 
				$masuk = $masuk+$row['bebas_pay_bill'];		
				?>
				<tr>
 					<td><?=$no;?></td>
 					<td><?=$row['pos_name'].' - T.A '.$row['period_start'].'/'.$row['period_end']?></td>
 					<td><?=$row['student_full_name']?></td>
 					<td><?=$row['class_name']?></td>
 					<td><?=pretty_date($row['bebas_pay_input_date'], 'm/d/Y', FALSE)?></td>
 					<td>Rp. <?=number_format($row['bebas_pay_bill'], 0, ',', '.')?></td>
 					<td>-</td>
 					<td><?=$row['bebas_pay_desc']?></td>
 				</tr>		
				<?php	
				$no++;
				}
				?>
				<?php
				foreach ($kredit as $val) { 
				$keluar = $keluar+$val['kredit_value'];	
				?>
				<tr>
					<td><?=$no;?></td>
					<td><?=$val['kredit_desc']?></td>
					<td>-</td>
					<td>-</td>
					<td><?=pretty_date($val['kredit_date'], 'm/d/Y', FALSE)?></td>
					<td>-</td>
					<td>Rp. <?=number_format($val['kredit_value'], 0, ',', '.')?></td>
					<td>-</td>
				</tr>
				<?php
				$no++;					
				}
				?>
				<?php
				foreach ($debit as $value) { 
				$masuk = $masuk+$value['debit_value'];		
				?>
				<tr>
					<td><?=$no;?></td>
					<td><?=$value['debit_desc']?></td>
					<td>-</td>
					<td>-</td>
					<td><?=pretty_date($value['debit_date'], 'm/d/Y', FALSE)?></td>
					<td>Rp. <?=number_format($value['debit_value'], 0, ',', '.')?></td>
					<td>-</td>
					<td>-</td>
				</tr>
				<?php
				$no++;					
				}
				?>
				<tr>
					<td colspan="5" align="center">Jumlah</td>
					<td>Rp. <?=number_format($masuk, 0, ',', '.')?></td>
					<td>Rp. <?=number_format($keluar, 0, ',', '.')?></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="5"></td>
					<td align="center" colspan="2">Rp. <?=number_format($masuk-$keluar, 0, ',', '.')?></td>
					<td colspan="1"></td>
				</tr>
			</tbody>
		</table>
					</div>
				</div>
			</div>

		</div>
	</section>
	<!-- /.content -->

</div>