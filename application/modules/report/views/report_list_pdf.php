<!DOCTYPE html>
<html>
<head>
	<title><?=$title?></title>
	<link rel="stylesheet" href="<?php echo media_url() ?>/css/bootstrap.min.css">
	<style type="text/css">
		* {
			font-size: 7px;
		}
	</style>
</head>
<body>
	
<table class="table table-responsive table-hover table-bordered" style="white-space: nowrap;">
	<tr>
		<th rowspan="2">Kelas</th> 
		<th rowspan="2">Nama</th>
		<?php foreach ($py as $row) : ?>
			<th colspan="<?php echo count($month) ?>"><center><?php echo $row['pos_name'].' - T.A '.$row['period_start'].'/'.$row['period_end']; ?></center></th>
		<?php endforeach ?>
		<?php foreach ($bebas as $key) : ?>
			<th rowspan="2"><?php echo $key['pos_name'].' - T.A '.$key['period_start'].'/'.$key['period_end']; ?></th>
		<?php endforeach ?>
	</tr>
	<tr>
		<?php foreach ($month as $key) : ?>
			<th><?php echo $key['month_name'] ?></th>
		<?php endforeach ?>
	</tr>
	<?php foreach ($student as $row) : ?>
	<tr>
		<td><?php echo $row['class_name']?></td> 
		<td><?php echo $row['student_full_name']?></td>
		<?php foreach ($bulan as $key) : ?>
			<?php if ($key['student_student_id']==$row['student_student_id']) { ?>
			<td style="color:<?php echo ($key['bulan_status']==1) ? '#00E640' : 'red' ?>"><?php echo ($key['bulan_status']==1) ? 'Lunas' : number_format($key['bulan_bill'], 0, ',', '.') ?></td>
			<?php } ?>
		<?php endforeach ?>
		<?php foreach ($free as $key) : ?>
			<?php if ($key['student_student_id']==$row['student_student_id']) { ?>
			<td style="text-align:center;color:<?php echo ($key['bebas_bill']==$key['bebas_total_pay']) ? '#00E640' : 'red' ?> "><?php echo ($key['bebas_bill']==$key['bebas_total_pay'])? 'Lunas' : number_format($key['bebas_bill']-$key['bebas_total_pay'],0,',','.') ?></td>
			<?php } ?>
		<?php endforeach ?>
	</tr>
	<?php endforeach ?>
</table>
<script src="<?php echo media_url() ?>/js/bootstrap.min.js"></script>
</body>
</html>