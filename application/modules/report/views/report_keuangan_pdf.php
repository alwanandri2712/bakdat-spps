<!DOCTYPE html>
<html>
<head>
	<title>Laporangan Keuangan</title>
	<link rel="stylesheet" href="<?php echo media_url() ?>/css/bootstrap.min.css">
	<style type="text/css">
		* {
			font-size: 7px;
		}
	</style>
</head>
<body>
<div class="container">
	<div class="row col-md-12">		
		<table class="table table-responsive">
			<tr>
				<td colspan="2" style="text-align: center;font-size: 9px;">Laporan Keuangan</td>
			</tr>
			<tr>
				<td colspan="2"><?=$setting_school['setting_value']?></td>
			</tr>
			<tr>
				<td>Tanggal Laporan</td>
				<td><?=pretty_date($q['ds'],'d F Y',false).' s/d '.pretty_date($q['de'],'d F Y',false)?></td>
			</tr>
			<tr>
				<td>Tanggal Unduh</td>
				<td><?=pretty_date(date('Y-m-d h:i:s'),'d F Y, H:i',false)?></td>
			</tr>
			<tr>
				<td>Pengunduh</td>
				<td><?=$this->session->userdata('ufullname')?></td>
			</tr>
		</table>
		<table class="table table-responsive table-hover table-bordered" style="white-space: nowrap;">
			<thead>
				<tr>
					<th>NO</th>
					<th>PEMBAYARAN</th>
					<th>NAMA SISWA</th>
					<th>KELAS</th>
					<th>TANGGAL</th>
					<th>PENERIMAAN</th>
					<th>PENGELUARAN</th>
					<th>KETERANGAN</th>
				</tr>		
			</thead>
			<tbody>
				<?php 
				$no=1;
				$masuk = 0;
				$keluar = 0;
				foreach ($bulan as $key) { 
				$masuk = $masuk+$key['bulan_bill'];	
				?>
 				<tr>
 					<td><?=$no;?></td>
 					<td><?=$key['pos_name'].' - T.A '.$key['period_start'].'/'.$key['period_end'].'-'.$key['month_name']?></td>
 					<td><?=$key['student_full_name']?></td>
 					<td><?=$key['class_name']?></td>
 					<td><?=pretty_date($key['bulan_date_pay'], 'm/d/Y', FALSE)?></td>
 					<td>Rp. <?=number_format($key['bulan_bill'], 0, ',', '.')?></td>
 					<td>-</td>
 					<td>-</td>
 				</tr>		
				<?php	
				$no++;	
				}
				?>
				<?php
				foreach ($free as $row) { 
				$masuk = $masuk+$row['bebas_pay_bill'];		
				?>
				<tr>
 					<td><?=$no;?></td>
 					<td><?=$row['pos_name'].' - T.A '.$row['period_start'].'/'.$row['period_end']?></td>
 					<td><?=$row['student_full_name']?></td>
 					<td><?=$row['class_name']?></td>
 					<td><?=pretty_date($row['bebas_pay_input_date'], 'm/d/Y', FALSE)?></td>
 					<td>Rp. <?=number_format($row['bebas_pay_bill'], 0, ',', '.')?></td>
 					<td>-</td>
 					<td><?=$row['bebas_pay_desc']?></td>
 				</tr>		
				<?php	
				$no++;
				}
				?>
				<?php
				foreach ($kredit as $val) { 
				$keluar = $keluar+$val['kredit_value'];	
				?>
				<tr>
					<td><?=$no;?></td>
					<td><?=$val['kredit_desc']?></td>
					<td>-</td>
					<td>-</td>
					<td><?=pretty_date($val['kredit_date'], 'm/d/Y', FALSE)?></td>
					<td>-</td>
					<td>Rp. <?=number_format($val['kredit_value'], 0, ',', '.')?></td>
					<td>-</td>
				</tr>
				<?php
				$no++;					
				}
				?>
				<?php
				foreach ($debit as $value) { 
				$masuk = $masuk+$value['debit_value'];		
				?>
				<tr>
					<td><?=$no;?></td>
					<td><?=$value['debit_desc']?></td>
					<td>-</td>
					<td>-</td>
					<td><?=pretty_date($value['debit_date'], 'm/d/Y', FALSE)?></td>
					<td>Rp. <?=number_format($value['debit_value'], 0, ',', '.')?></td>
					<td>-</td>
					<td>-</td>
				</tr>
				<?php
				$no++;					
				}
				?>
				<tr>
					<td colspan="5" align="center">Jumlah</td>
					<td>Rp. <?=number_format($masuk, 0, ',', '.')?></td>
					<td>Rp. <?=number_format($keluar, 0, ',', '.')?></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="5"></td>
					<td align="center" colspan="2">Rp. <?=number_format($masuk-$keluar, 0, ',', '.')?></td>
					<td colspan="1"></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
</body>
</html>